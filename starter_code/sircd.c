/*
TODO: 
- Send message of the day when user is registered
- Reverse lookup for hostname
- Nicknames should be case insensitive
- Responses from server need to be formatted correctly, including errors
- Buffer messages greater than receiving size
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <assert.h>
#include "debug.h"
#include "rtlib.h"
#include "sircd.h"
#include "irc_proto.h"

u_long curr_nodeID;
rt_config_file_t   curr_node_config_file;  /* The config_file  for this node */
rt_config_entry_t *curr_node_config_entry; /* The config_entry for this node */
fd_set masterSet;

void init_node(char *nodeID, char *config_file);
void irc_server();
void HandleClient(int sock);
void HandleMessage(char *message, int sock);
void HandleCommand(char *message, int index, int sock);

void JoinCommand(char *message, int index, int sock);
void QuitCommand(char *message, int index, int sock);
void WhoCommand(char *message, int index, int sock);
void NickCommand(char *message, int index, int sock);
void UserCommand(char *message, int index, int sock);
void PrivmsgCommand(char *message, int index, int sock);
void ListCommand(char *message, int index, int sock);
void PartCommand(char *message, int index, int sock);

void SendToClient(char *message, int len, int sock);
client clients[MAX_CLIENTS];
int numberOfClients = 0;

int clientIndex(int sock);
int charIndex(char *message, int startIndex, char searchChar);


void SendToChannel(char *message, int len, char *channelname, int channelNameLength, int ignoreSock);

void
usage() {
    fprintf(stderr, "sircd [-h] [-D debug_lvl] <nodeID> <config file>\n");
    exit(-1);
}

int main( int argc, char *argv[] )
{
    extern char *optarg;
    extern int optind;
    int ch;
    
    while ((ch = getopt(argc, argv, "hD:")) != -1)
        switch (ch) {
	case 'D':
	    if (set_debug(optarg)) {
		exit(0);
	    }
	    break;
        case 'h':
        default: /* FALLTHROUGH */
            usage();
        }
    argc -= optind;
    argv += optind;

    if (argc < 2) {
	usage();
    }
    
    init_node(argv[0], argv[1]);
    
    printf("I am node %lu and I listen on port %d for new users and at IP %lu\n", curr_nodeID, curr_node_config_entry->irc_port, curr_node_config_entry->ipaddr );

    /* Start your engines here! */
	int listenSock, newSock;
	//sets for storing all sockets and 'currently' readable sockets
	fd_set readSet;
	//max fd for call to select()
	int maxFd;

	struct addrinfo hints, client, *res, *p;
	memset(&hints, 0, sizeof(hints));
	memset(&client, 0, sizeof(client));
	//IPv4 only
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_flags = AI_PASSIVE;

	char portstr[6];
	sprintf(portstr, "%d", curr_node_config_entry -> irc_port);

	if(getaddrinfo(NULL, portstr, &hints, &res) < 0){
	//if(getaddrinfo(NULL, inet_ntop(AF_INET, htons(curr_node_config_entry -> irc_port)), &hints, &res) < 0){
		printf("getaddrinfo error");
	}

	//iterate through sockaddr linked list, select first socket that successfully binds
	for(p = res; p != NULL; p = (p -> ai_next)){
		if((listenSock = socket(p -> ai_family, p -> ai_socktype, p -> ai_protocol)) < 0){
			printf("Socket creation error");
			continue;
		}

		if((bind(listenSock, p -> ai_addr, p -> ai_addrlen)) < 0){
			printf("Failed to bind");
			close(listenSock);
			continue;
		}
		break;
	}

	//end of linked list reached without successful bind
	if(p == NULL){
		printf("Bind failed");
	} 
	
	//free results addrinfo
	freeaddrinfo(res);

	if((listen(listenSock, SOMAXCONN)) < 0){
		printf("Failed to listen");
	}

	memset(&masterSet, 0, sizeof(masterSet));
	memset(&readSet, 0, sizeof(readSet));

	//add currently listening socket to masterSet
	FD_SET(listenSock, &masterSet);

	//update maxFd
	maxFd = listenSock;

	while(1){
		readSet = masterSet;

		printf("maxFd: %d\n", maxFd);
		printf("listenSock: %d\n", listenSock);
		fflush(stdout);
		if((select(maxFd+1, &readSet, NULL, NULL, NULL)) < 0){
			printf("Select error");
		}

		//iterate up to maxFd to determine all fds in readSet, and read from them
		int x;
		for(x = 0; x < (maxFd+1); x++){	
			//check if fd is in readSet, otherwise continue with loop
			if(FD_ISSET(x, &readSet)){
				//if listenSock is in readSet, accept new connection, otherwise read from socket
				if(x == listenSock){	
					//accept connection
					if((newSock = accept(x, client.ai_addr, &client.ai_addrlen)) < 0){
						printf("accept connection error");
					}

					printf("Client connected: %d\n", newSock);
					//printf("Client connected: %s\n", inet_ntoa(((struct sockaddr_in *)(client.ai_addr)) -> sin_addr));

					//add new socket to masterSet
					FD_SET(newSock, &masterSet);
			
					//create client
					clients[numberOfClients].sock = newSock;
					//clients[numberOfClients].cliaddr = *((struct sockaddr_in *)client.ai_addr);
					clients[numberOfClients].registered = 0;
					strcpy(clients[numberOfClients].user, "");
					strcpy(clients[numberOfClients].nick, "");
					strcpy(clients[numberOfClients].realname, "");
					strcpy(clients[numberOfClients].channel, "");
					numberOfClients += 1;
	
					/*
					//reverse hostname lookup
					struct hostent *hostinfo; 
					hostinfo = gethostbyaddr(&client.ai_addr,sizeof(&client.ai_addr), AF_INET);
					strcpy(clients[numberOfClients].hostname, hostinfo -> h_name);
					*/

					if(newSock > maxFd){
						maxFd = newSock;
					}
				}
				else{
					printf("Message from:%d\n", x);
					fflush(stdout);

					HandleClient(x);			
				}
			}
		}
	}

    return 0;

}

/*
 * Accept message from client
 */
void HandleClient(int sock){
	char buffer[MAX_MSG_LEN];
	int received = -1;

	//receive bytes from client
	if((received = recv(sock, buffer, MAX_MSG_LEN, 0)) < 0){
		printf("Failed to receive\n");
	}
		
	HandleMessage(buffer, sock);

/*
	//send bytes to client and check for any further messages
	while(received > 0){
		//send received bytes
			
		if((send(sock, buffer, received, 0)) != received){
			printf("Failed to send\n");
		}
		
		//check for additional messages
		if((received = recv(sock, buffer, MAX_MSG_LEN, 0)) < 0){
			printf("No additional bytes from client\n");
		}

		HandleMessage(buffer, sock);
	}
*/
	//close(sock);
}

/*
 * Parse message and determine what to do with it
 */

void HandleMessage(char *message, int sock){
	char *command;
	int commandIndex = 0;
	int prefixExists = 0;

	//if prefix exists, find where command starts
	if(message[0] == ':'){
		prefixExists = 1;
		for(int x = 0; x < MAX_MSG_LEN; x++){
			if(message[x] == ' '){
				commandIndex = x+1;
				break;
			}
		}
	}
	
	HandleCommand(message, commandIndex, sock);
}

/*
 * Handle command in message
 */
void HandleCommand(char *message, int index, int sock){
	char *command = message+index;

	int location = clientIndex(sock);

	if(clients[location].registered == 0){
			if((strncmp(command, "NICK", 4) == 0) && ((message[index+4] == '\r') || (message[index+4] == ' '))){
				NickCommand(message, index+4, sock);
			}
			else if((strncmp(command, "USER", 4) == 0) && ((message[index+4] == '\r') || (message[index+4] == ' '))){
				UserCommand(message, index+4, sock);
			}
			else{
				SendToClient("ERR_NOTREGISTERED\n",18, sock);
				fflush(stdout);
			}
	}
	else{
			if((strncmp(command, "NICK", 4) == 0) && ((message[index+4] == '\r') || (message[index+4] == ' '))){
				NickCommand(message, index+4, sock);
			}
			else if((strncmp(command, "USER", 4) == 0) && ((message[index+4] == '\r') || (message[index+4] == ' '))){
				UserCommand(message, index+4, sock);
			}
			else if((strncmp(command, "QUIT", 4) == 0) && ((message[index+4] == '\r') || (message[index+4] == ' '))){
				QuitCommand(message, index+4, sock);
			}
			else if((strncmp(command, "JOIN", 4) == 0) && ((message[index+4] == '\r') || (message[index+4] == ' '))){
				JoinCommand(message, index+4, sock);
			}
			else if((strncmp(command, "PART", 4) == 0 && ((message[index+4] == '\r') || (message[index+4] == ' ')))){
				PartCommand(message, index+4, sock);
			}
			else if((strncmp(command, "LIST", 4) == 0) && ((message[index+4] == '\r') || (message[index+4] == ' '))){
				ListCommand(message, index+4, sock);
				printf("LIST COMMAND\n");
				fflush(stdout);
			}
			else if((strncmp(command, "PRIVMSG", 7) == 0) && ((message[index+7] == '\r') || (message[index+7] == ' '))){
				PrivmsgCommand(message, index+7, sock);
			}
			else if((strncmp(command, "WHO", 3) == 0) && ((message[index+3] == '\r') || (message[index+3] == ' '))){
				WhoCommand(message, index+3, sock);
			}
			else{
				//unkown command, return ERR_UNKNOWNCOMMAND
				SendToClient("ERR_UNKNOWNCOMMAND\n", 19, sock); 
				printf("ERR_UNKNOWNCOMMAND\n");
				fflush(stdout);
			}
	}
	return;
}


void NickCommand(char *message, int index, int sock){
	if(message[index] != ' '){
		//invalid format
		SendToClient("ERR_NEEDMOREPARAMS\n", 19, sock);
		return;
	}

	//increment index to start of parameters
	index+=1;

	//determine end of nickname, ignore hopcount
	int y = charIndex(message, index, ' ');
	int r = charIndex(message, index, '\r');
	
	if(y == -1){
		y = r;
	}

	int size = y-index;
	//check to ensure nickname is less than or equal to allowed size
	if(size>MAX_USERNAME){
		//return error
		SendToClient("Nickname too large\n", 19, sock);
	}
	
	//char *nickname = "";
	char nickname[size];
	nickname[0] = '\0';
	strncat(nickname, message+index, size);

	//check for collisions
	int z;
	for(z = 0; z < numberOfClients; z++){
		if(strcmp(nickname, clients[z].nick) == 0){
			SendToClient("ERR_NICKCOLLISION\n", 18, sock);
			return;
		}
		else{
			printf("Length of requested nickname: %d\n", strlen(nickname));
			printf("Size: %d\n", size);
			printf("Length of client nickname: %d\n", strlen(clients[z].nick));
			fflush(stdout);
		}
	}
	
	int location = clientIndex(sock);

	//erase old nickname
	memset(clients[location].nick, 0, MAX_USERNAME);

	if(clients[location].registered){
		//SendToClient("Nickname changed alert for registered user\n", 43, sock);
		SendToChannel("Nickname changed alert for registered user\n", 43, clients[location].channel, strlen(clients[location].channel), clients[location].sock);
	}
	else if(strlen(clients[location].user) != 0){
		//user command already called
		clients[location].registered = 1;
	}	
	strncpy(clients[location].nick, message+index, size);
	return;
}


void UserCommand(char *message, int index, int sock){
	if(message[index] != ' '){
		//invalid format
		SendToClient("ERR_NEEDMOREPARAMS\n", 19, sock);
		return;
	}

	//increment index to start of parameters
	index+=1;

	//determine end of username
	int endOfUsername = charIndex(message, index, ' ');
	if(endOfUsername < 0){
		//invalid format
		SendToClient("ERR_NEEDMOREPARAMS\n", 19, sock);
		return;
	}

	//determine end of hostname
	int endOfHostname = charIndex(message, endOfUsername+1, ' ');
	if(endOfHostname < 0){	
		//invalid format
		SendToClient("ERR_NEEDMOREPARAMS\n", 19, sock);
		return;
	}

	//determine end of servername	
	int endOfServername = charIndex(message, endOfHostname+1, ' ');
	if(endOfServername < 0){	
		//invalid format
		SendToClient("ERR_NEEDMOREPARAMS\n", 19, sock);
		return;
	}

	printf("%d %d %d\n", endOfUsername, endOfHostname, endOfServername);
	int usernameSize = endOfUsername - index;
	int hostnameSize = endOfHostname - (endOfUsername + 1);
	int servernameSize = endOfServername - (endOfHostname + 1);

	printf("%d %d %d\n", usernameSize, hostnameSize, servernameSize);
	fflush(stdout);
	
	if((usernameSize > MAX_USERNAME) || (hostnameSize > MAX_HOSTNAME) || (servernameSize > MAX_SERVERNAME)){
		//return error
		SendToClient("ERR_INVALID\n", 11, sock);
		return;
	}

	char username[usernameSize];
	char servername[servernameSize];

	strncpy(username, message+index, usernameSize);
	strncpy(servername, message+endOfHostname+1, servernameSize);

	//determine start of realname
	int startOfRealname = charIndex(message, index, ':');
	
	if(startOfRealname < 0){
		//invalid format
		SendToClient("ERR_NEEDMOREPARAMS\n", 19, sock);
		return;
	}
	
	startOfRealname += 1;
	if(message[startOfRealname] == ' ' || message[startOfRealname] == '\r'){
		//invalid format
		SendToClient("ERR_NEEDMOREPARAMS\n", 19, sock);
		return;
	}

	//determine end of realname
	int endOfRealname = charIndex(message, index, '\r');
		
	int realnameSize = endOfRealname-startOfRealname;
	char realname[realnameSize];	
	strncpy(realname, message+startOfRealname, realnameSize);

	int location = clientIndex(sock);
	
	//erase old username, realname, and servername
	memset(clients[location].user, 0, MAX_USERNAME);	
	memset(clients[location].realname, 0, MAX_REALNAME);
	memset(clients[location].servername, 0, MAX_SERVERNAME);

	strncpy(clients[location].user, username, usernameSize);
	strncpy(clients[location].realname, realname, realnameSize);
	strncpy(clients[location].servername, servername, servernameSize);

	if((clients[location].registered == 0) && (strlen(clients[location].nick) != 0)){
		//nickname command already called
		clients[location].registered = 1;
	}

	printf("start:%d end:%d size:%d\n", startOfRealname, endOfRealname, realnameSize);
	return;
}

void QuitCommand(char *message, int index, int sock){	
	int arrayIndex = clientIndex(sock);

	int messageStart = charIndex(message, index, ':');
	
	char quitMessage[MAX_MSG_LEN];
	//if message is provided, send message, otherwise nickname
	if(messageStart > 0){
		int messageEnd = charIndex(message, index, '\r');
		strncpy(quitMessage, message+messageStart+1, messageEnd-messageStart);
	}
	else{
		strcpy(quitMessage, clients[arrayIndex].nick);
	}

	char str[MAX_MSG_LEN];
	sprintf(str, ":%s!%s@%s QUIT:%s", clients[arrayIndex].nick, clients[arrayIndex].user, clients[arrayIndex].hostname, quitMessage);
	SendToChannel(str, strlen(str), clients[arrayIndex].channel, strlen(clients[arrayIndex].channel), -1); 	
/*
	SendToChannel("Client ", 7, clients[arrayIndex].channel,strlen(clients[arrayIndex].channel), clients[arrayIndex].sock);
	SendToChannel(clients[arrayIndex].nick, strlen(clients[arrayIndex].nick), clients[arrayIndex].channel, strlen(clients[arrayIndex].channel), clients[arrayIndex].sock);
	SendToChannel(" has quit the server\n", 21, clients[arrayIndex].channel, strlen(clients[arrayIndex].channel), clients[arrayIndex].sock);
*/

	int x;
	for(x = arrayIndex; x < (numberOfClients-1); x++){
		clients[x] = clients[x+1];
	}

	static const client emptyClient;
	clients[numberOfClients] = emptyClient;
	numberOfClients -= 1;

	FD_CLR(sock, &masterSet);
	close(sock);	
	return;
}

void JoinCommand(char *message, int index, int sock){
	if(message[index] != ' '){
		//invalid format
		SendToClient("ERR_NEEDMOREPARAMS\n", 19, sock);
		return;
	}
	//increment to start of parameters;
	index+=1;

	if(message[index] != '#' && message[index] != '&'){	
		SendToClient("Error, invalid channel format", 30, sock);
	}

	//determine end of channelname
	int end = charIndex(message, index, '\r');
	
	int location = clientIndex(sock);

	//erase previous channel name
	memset(clients[location].channel, 0, MAX_CHANNAME);

	strncpy(clients[location].channel, message+index, end-index);
	return;
}

void PartCommand(char *message, int index, int sock){
	if(message[index] != ' '){
		//invalid format
		SendToClient("ERR_NEEDMOREPARAMS\n", 19, sock);
		return;
	}

	//increment to start of parameters;
	index+=1;

	if(message[index] != '#' && message[index] != '&'){	
		SendToClient("Error, invalid channel format", 30, sock);
	}

	//determine end of channelname
	int end = charIndex(message, index, '\r');
	
	int location = clientIndex(sock);

	char channelname[end-index];
	channelname[0] = '\0';
	//strncpy(channelname, message+index, end-index-1);
	strncat(channelname, message+index, end-index);
	
	if(strcmp(clients[location].channel, channelname) == 0){
		char str[MAX_MSG_LEN];
		sprintf(str, ":%s!%s@%s QUIT: %s %s", clients[location].nick, clients[location].user, clients[location].hostname, "Parted channel", clients[location].channel);
		SendToChannel(str, strlen(str), clients[location].channel, end-index, -1); 	
		memset(clients[location].channel, 0, MAX_CHANNAME);
	}
	else{
		SendToClient("Not connected to channel\n", 25, sock);
		printf("channelname:%s size:%d, channel:%s size:%d\n", channelname, strlen(channelname), clients[location].channel, strlen(clients[location].channel));
		fflush(stdout);	
	}
	return;
}

void ListCommand(char *message, int index, int sock){	
	//ignore parameters even if provided

	int x;
	for(x = 0; x < numberOfClients; x++){
		int number = 0;
		int print = 1;

		char channelname[MAX_CHANNAME] = {"0"};
		strcpy(channelname, clients[x].channel);

		if(strlen(channelname) == 0){
			continue;
		}

		int y;
		for(y = 0; y < numberOfClients; y++){
			if(strcmp(channelname, clients[y].channel) == 0){
				//channel already printed, do not print again
				if(y < x){
					print = 0;
				}
				number+=1;
			}
		}
		if(print){
				char buffer[2];
				sprintf(buffer, "%d", number);
				SendToClient("Channel:", 8, sock);
				SendToClient(channelname, strlen(channelname), sock);
				SendToClient("\n", 1, sock);
				SendToClient("Number:", 7, sock);
				SendToClient(buffer, 2, sock);		
				SendToClient("\n", 1, sock);
		}
	}
	return;
}

void PrivmsgCommand(char *message, int index, int sock){
	if(message[index] != ' '){
		//invalid format
		SendToClient("ERR_NEEDMOREPARAMS\n", 19, sock);
		return;
	}
	//increment index to first parameter
	index += 1;

	//size of message
	int size = 0;
	while(message[size] != '\r'){
		size++;
	}

	int startOfText = charIndex(message, index, ':');
	if((startOfText < 0) || (message[index] == ' ') || (message[index] == ':') || (message[index] == '\r')){
		SendToClient("ERR_NEEDMOREPARAMS\n", 19, sock);
		return;
	}
	
	int arrayIndex = clientIndex(sock);

	while((message[index] != ':') && (index < size)){
			if (message[index] == '#' || message[index] == '&'){
				//target is channel
				int iterator = index;
				while((message[iterator] != ' ') && (message[iterator] != ',')){
					iterator += 1;
				}

				char str[MAX_MSG_LEN];
				sprintf(str, ":%s %s", clients[arrayIndex].nick, message);
 	
				SendToChannel(str, strlen(str), message+index, iterator - index, sock); 
				index = iterator+1;
			}
			else{
				//target is client
				int endIndexComma = charIndex(message, index, ',');
				int endIndexSpace = charIndex(message, index, ' ');
				int endIndex;

				if((endIndexComma < endIndexSpace) && (endIndexComma != -1)){
					endIndex = endIndexComma;
				}
				else if(endIndexSpace != -1){
					endIndex = endIndexSpace;
				}
				else{
					//incorrect formatting
					SendToClient("ERR_NEEDMOREPARAMS\n", 19,sock);
				}

				//char *nickname = message+index;
				
				char nickname[endIndex-index];
				nickname[0] = '\0';
				//strncpy(channelname, message+index, end-index-1);
				strncat(nickname, message+index, endIndex-index);
				
				int sent = 0;
				int x;
				for(x = 0; x < numberOfClients; x++){
					if(strcmp(clients[x].nick, nickname) == 0){
	
						char str[MAX_MSG_LEN];
						sprintf(str, ":%s %s", clients[arrayIndex].nick, message);
						SendToClient(str, strlen(str), clients[x].sock); 	
						/*
						SendToClient(message, size, clients[x].sock);
						SendToClient("\n", 1, clients[x].sock);
						*/
						sent = 1;
					}
				}

				if(sent == 0){
					SendToClient("ERR_NOSUCHNICK", 14, sock);
				}

				index = endIndex+1;
			}
	}
	return;
}

void WhoCommand(char *message, int index, int sock){
	if(message[index] != ' '){
		//invalid format
		SendToClient("ERR_NEEDMOREPARAMS\n", 19, sock);
		return;
	}
	index +=1;

	int endIndex = charIndex(message, index, '\r');
	char channelName[endIndex-index];
	channelName[0] = '\0';
	strncat(channelName, message+index, endIndex-index);

	client clientsConnected[MAX_CLIENTS];
	
	SendToClient("Users connected to channel:\n", 28, sock);
	int x;
	for(x = 0; x < numberOfClients; x++){
		if(strcmp(clients[x].channel, channelName) == 0){
			SendToClient(clients[x].nick, strlen(clients[x].nick), sock);
			SendToClient("\n", 1, sock);
		}
	}
	return;
}

void SendToClient(char *message, int len, int sock){
	char buffer[MAX_MSG_LEN];
	strcpy(buffer, message);
	int received = len;
	if((send(sock, buffer, received, 0)) <= 0){
		printf("Failed to send\n");
	}
}

void SendToChannel(char *message, int len, char *channelname, int channelNameLength, int ignoreSock){
	int x;
	for(x = 0; x < numberOfClients; x++){
		char channelName[channelNameLength];	
		channelName[0] = '\0';
		strncat(channelName, channelname, channelNameLength);
		if(strcmp(clients[x].channel, channelName) == 0){
			if(clients[x].sock != ignoreSock){
				SendToClient(message, len, clients[x].sock);
			}
		}
	}
	return;
}

int clientIndex(int sock){
	int x;
	for(x = 0; x < numberOfClients; x++){
		if(clients[x].sock == sock){
			return x;
		}
	}
	return -1;
}

int charIndex(char *message, int startIndex, char searchChar){
	int x;
	for(int x = startIndex; x < MAX_MSG_LEN; x++){
		if(message[x] == searchChar){
			return x;
		}
		else if(message[x] == '\r'){
			break;
		}
	}
	return -1;
}
/*
 * void init_node( int argc, char *argv[] )
 *
 * Takes care of initializing a node for an IRC server
 * from the given command line arguments
 */
void
init_node(char *nodeID, char *config_file) 
{
    int i;

    curr_nodeID = atol(nodeID);
    rt_parse_config_file("sircd", &curr_node_config_file, config_file );

    /* Get config file for this node */
    for( i = 0; i < curr_node_config_file.size; ++i )
        if( curr_node_config_file.entries[i].nodeID == curr_nodeID )
             curr_node_config_entry = &curr_node_config_file.entries[i];

    /* Check to see if nodeID is valid */
    if( !curr_node_config_entry )
    {
        printf( "Invalid NodeID\n" );
        exit(1);
    }
}

